package main

import (
	"encoding/json"
	"errors"
	"flag"
	"io"
	"io/fs"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"

	"github.com/mmcdole/gofeed"
)

var (
	feedsFile   = flag.String("feedsFile", "", "")
	downloadDir = flag.String("downloadDir", "", "")
	targetDir   string
)

func main() {
	flag.Parse()
	targetDir = *downloadDir + "/torrents"
	err := os.MkdirAll(targetDir, 0755)
	if err != nil {
		log.Fatal(err)
	}
	feeds := loadFeeds()
	now := time.Now()
	fp := gofeed.NewParser()
	for _, query := range feeds {
		feed, err := fp.ParseURL("https://nyaa.si/?page=rss" + query)
		if err != nil {
			log.Fatal(err)
		}
		for _, item := range feed.Items {
			if now.Sub(*item.PublishedParsed) < 30*24*time.Hour {
				log.Print("pondering ", item.Title)
				torrentUrl, err := url.Parse(item.Link)
				if err != nil {
					log.Fatal(err)
				}
				filename := path.Base(torrentUrl.Path)
				_, err = os.Stat(path.Join(targetDir, filename))
				if err != nil && !errors.Is(err, fs.ErrNotExist) {
					log.Fatal(err)
				}
				if errors.Is(err, fs.ErrNotExist) {
					log.Print("guess i'll download ", item.Title)
					download(path.Join(targetDir, filename), item.Link)
				}
			}
		}
	}
}

func loadFeeds() (ret []string) {
	f, err := os.Open(*feedsFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	dec := json.NewDecoder(f)
	err = dec.Decode(&ret)
	if err != nil {
		log.Fatal(err)
	}
	return
}

func download(filename, url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	temp, err := os.CreateTemp(*downloadDir, "")
	if err != nil {
		log.Fatal(err)
	}
	_, err = io.Copy(temp, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	err = temp.Sync()
	if err != nil {
		log.Fatal(err)
	}
	err = os.Rename(temp.Name(), filename)
	if err != nil {
		log.Fatal(err)
	}
	temp.Close()
}
